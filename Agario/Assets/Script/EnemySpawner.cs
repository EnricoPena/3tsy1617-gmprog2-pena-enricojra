﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {
    public GameObject Enemy;
    public float spawn;
    void Start()
    {
            InvokeRepeating("Respawn", 0, spawn);
    }
   
    void Respawn()
    {
        int x = Random.Range(-50,50);
        int y = Random.Range(-50,50);
		Vector3 Target = new Vector3 (x, y, 0);
		Instantiate(Enemy, Target, Quaternion.identity);

        
    }

}
