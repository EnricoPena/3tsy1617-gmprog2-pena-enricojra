﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	private float Scale;
	public GameObject MainCamera;
	private Vector3 offset;
	public float Speed;

	private float CamSize;
	// Use this for initialization
	void Start()
	{
		offset = transform.position - MainCamera.transform.position;
		//Scale = MainCamera.GetComponent<PlayerCollision> ().scale;
		CamSize = Camera.main.orthographicSize;
	}
	// Update is called once per frame
	void LateUpdate()
	{
		


		//if(MainCamera!=null)
		Scale = MainCamera.GetComponent<PlayerCollision> ().scale;
		transform.position = Vector3.MoveTowards (transform.position, MainCamera.transform.position, Speed * Time.deltaTime);

		//if (MainCamera.transform.localScale.x > Scale) {
		//Camera.main.orthographicSize += MainCamera.transform.position.x;
		//Camera.main.orthographicSize += Scale;

		//Scale = MainCamera.transform.localScale.x;

		//if (MainCamera.transform.localScale.x < Scale) {
		//Camera.main.orthographicSize -= Scale;

		//Scale = MainCamera.transform.localScale.x;

		Camera.main.orthographicSize = Mathf.Lerp(CamSize, Scale, Time.deltaTime * 5);	


			}

	
}