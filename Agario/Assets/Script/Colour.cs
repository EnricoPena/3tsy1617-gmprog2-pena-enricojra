﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Colour : MonoBehaviour {

    public List<Material> mats = new List<Material>();
    void Awake()
    {
        // call once created
        GetComponent<Renderer>().material = mats[Random.Range(0,mats.Count)];
    }

}
