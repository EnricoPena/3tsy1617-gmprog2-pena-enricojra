﻿using UnityEngine;

using System.Collections;

public class Player : MonoBehaviour {
	public float Speed;


	void Update () {
		Vector3 Target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Target.z = transform.position.z; 
		transform.position = Vector3.Lerp(transform.position,Target,Speed*Time.deltaTime/ transform.localScale.x);
		this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -50, 50), Mathf.Clamp(this.transform.position.y, -50, 50), this.transform.position.z);
		/*
		if (Input.GetKey (KeyCode.KeypadPlus)) 
		{
			transform.localScale += new Vector3 (1.0f * Time.deltaTime, 1.0f * Time.deltaTime, 1.0f * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.KeypadMinus)) 
		{
			this.transform.localScale += new Vector3 (-1.0f * Time.deltaTime, -1.0f * Time.deltaTime, -1.0f * Time.deltaTime);
		}*/
		if (Input.GetKeyDown (KeyCode.KeypadPlus)) {
			transform.localScale += new Vector3(1,1,1);
		}

		if (Input.GetKeyDown (KeyCode.KeypadMinus)) {
			transform.localScale -= new Vector3(1,1,1);
		}
	}


}
