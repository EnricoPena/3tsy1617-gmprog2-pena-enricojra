﻿using UnityEngine;
using System.Collections;

public class EnemyCollision : MonoBehaviour {

    public float scale;
    public string tags;


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == tags)
        {
            transform.localScale += new Vector3(scale, scale, scale);
            Camera.main.orthographicSize = Camera.main.orthographicSize + 0.05f;
            Destroy(other.gameObject);

        }
		if ((other.gameObject.tag == "Player") && this.transform.localScale.x > other.transform.localScale.x)
		{
			Destroy (other.gameObject);
		}

		if ((other.gameObject.tag == "Enemy") && this.transform.localScale.x > other.transform.localScale.x)
		{
			Destroy (other.gameObject);
		}


    }
}
