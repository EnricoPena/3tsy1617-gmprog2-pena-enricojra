﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyState : MonoBehaviour {

	public float Speed;
	GameObject Player;
	GameObject Food;

	float FoodDistance;
	float PlayerDistance;

	// Use this for initialization
	void Start()
	{
		Player = GameObject.FindGameObjectWithTag ("Player");

	}
	
	// Update is called once per frame
	void Update () {
		Food = FindClosestObj ("Food");

		FindDistance ();
		SetState ();
	}

	void SetState()
	{
		Patrol ();

		if ( Player.transform.localScale.x < gameObject.transform.localScale.x ) {
			Chase ();
		}

		if (PlayerDistance < FoodDistance && Player.transform.localScale.x > gameObject.transform.localScale.x) {
			Run ();
		}
	}
		
	void Patrol()
	{
		transform.position = Vector3.MoveTowards(transform.position, Food.transform.position, Speed * Time.deltaTime / transform.localScale.x);
		this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -50, 50), this.transform.position.y, this.transform.position.z);
		Debug.Log ("PATROL");
	}
		//if players smaller chase
	void Chase()
	{
		transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, Speed * Time.deltaTime);
		this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -50, 50), this.transform.position.y, this.transform.position.z);
		Debug.Log ("CHASING");

	}
		//if player is bigger run and eat food
	void Run()
	{
        /*transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, -Speed * Time.deltaTime / transform.localScale.x);
		this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -50, 50), this.transform.position.y, this.transform.position.z);*/
        Vector3 moveDir = this.transform.position - Player.transform.position;
        transform.Translate(moveDir.normalized * Speed * Time.deltaTime);
		Debug.Log ("RUNNING");
	}

	void FindDistance()
	{
		FoodDistance = Vector3.Distance(transform.position, FindClosestObj("Food").transform.position);
		PlayerDistance = Vector3.Distance(transform.position, FindClosestObj("Player").transform.position);
	}

	GameObject FindClosestObj(string Tag)
	{
		GameObject[] asd;
		asd = GameObject.FindGameObjectsWithTag(Tag);
		GameObject closest = null;

		float distance = Mathf.Infinity;
		//float distance = 10.0f;
		Vector3 position = transform.position;
		foreach (GameObject go in asd)
		{
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance)
			{
				closest = go;
				distance = curDistance;
			}
		}
		return closest;
	}
}
