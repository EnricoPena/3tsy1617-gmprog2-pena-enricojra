﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Node : MonoBehaviour
{    
    public Vector3 positionOffset;
    public Color hoverColor;
    public Color initialColor;
    public Text textBox;
    private Renderer rend;
    BuildManager buildManager;
        
    public GameObject tower;
    [HideInInspector]
    public TowerBlueprint towerBlueprint;
    [HideInInspector]
    public int upgradeLevel;
    void Start()
    {
        buildManager = BuildManager.instance;
        rend = GetComponent<Renderer>();
        initialColor = rend.material.color;
    }
    public Vector3 GetBuildPosition()
    {
        return transform.position + positionOffset;
    }
    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }  
        if (tower != null)
        {
            buildManager.SelectNode(this);
            return;
        }
        if (!buildManager.CanBuild)
        {
            return;
        }
        StartCoroutine(BuildTower(buildManager.GetTowerToBuild()));
    }
    public IEnumerator BuildTower(TowerBlueprint _towerBlueprint)
    {
        if (PlayerStatus.gold < _towerBlueprint.cost)
        {
            textBox.text = "Not enough gold.";
            yield break;
        }        
        PlayerStatus.gold -= _towerBlueprint.cost;            

        GameObject _tower = (GameObject)Instantiate(_towerBlueprint.towerPrefab, GetBuildPosition(), Quaternion.identity);        

        _tower.SetActive(false);
        GameObject effect = (GameObject)Instantiate(_towerBlueprint.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, _towerBlueprint.buildDelay);
        yield return new WaitForSeconds(_towerBlueprint.buildDelay);
        _tower.SetActive(true);
        
        // Setting variables for upgrade
        tower = _tower;
        towerBlueprint = _towerBlueprint;
    }
    
    public IEnumerator UpgradeTower()
    {
        if (upgradeLevel >= 2)
        {
            textBox.text = "Tower is at max upgrade!";
            yield break;
        }
        if ((PlayerStatus.gold < (towerBlueprint.upgradeCost * (1 + upgradeLevel))))
        {
            textBox.text = "Not enough gold.";
            yield break;
        }        

        PlayerStatus.gold -= towerBlueprint.upgradeCost * (1 + upgradeLevel);        
        
        // Destroy old tower
        Destroy(tower.gameObject);
        // Build new one
        GameObject _tower = (GameObject)Instantiate(towerBlueprint.upgradedPrefab[upgradeLevel], GetBuildPosition(), Quaternion.identity);
        _tower.SetActive(false);
        GameObject effect = (GameObject)Instantiate(towerBlueprint.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, towerBlueprint.upgradeDelay);
        yield return new WaitForSeconds(towerBlueprint.upgradeDelay);
        _tower.SetActive(true);
        tower = _tower;

        upgradeLevel++;
        textBox.text = "Tower upgraded!";
    }
    void OnMouseEnter()
    {
        if (!buildManager.CanBuild)
        {
            return;
        }
        if (buildManager.HasEnoughGold)
        {
            rend.material.color = hoverColor;
        }
        else
        {
            rend.material.color = Color.red;
        }        
    }
    void OnMouseExit()
    {
        rend.material.color = initialColor;
    }
}
    