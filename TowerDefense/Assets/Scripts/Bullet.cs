﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    private Transform target;
    public int damage = 50;
    public float speed = 70f;
    public float explosionRadius;
    public GameObject impactEffect;
    [Header("Special Towers")]
    public bool isIceTower = false;
    public bool isFireTower = false;
    public GameObject debuffPrefab;

	void Update () {
		if (target == null)
        {
            Destroy(gameObject);
            return;
        }
        Vector3 direction = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;
        if (direction.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(direction.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);
	}
    
    public void Seek (Transform _target)
    {
        target = _target;
    }
    void HitTarget()
    {        
        GameObject effect = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effect, 5f);
        if(explosionRadius > 0f)
        {            
            Explode();           
        }
        else if (explosionRadius == 0)
        {
            Damage(target);
        }
        Destroy(gameObject);
    }
    void Damage(Transform enemy)
    {
        Enemy e = enemy.GetComponent<Enemy>();

        if (e != null)
        {            
            e.TakeDamage(damage);
        }        
    }
    void Explode()
    {        
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Monster")
            {                
                Damage(collider.transform);
                if (isIceTower || isFireTower)
                {                    
                    GameObject debuff = Instantiate(debuffPrefab, collider.transform.position, collider.transform.rotation) as GameObject;
                    debuff.GetComponent<Debuff>().target = collider.transform.GetComponent<DebuffReceiver>();
                    debuff.transform.parent = collider.transform;
                }                                    
            }
        }
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
