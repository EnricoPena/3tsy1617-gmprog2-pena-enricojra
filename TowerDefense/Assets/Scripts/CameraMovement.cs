﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class Boundary
{
    public float MIN_X, MIN_Z, MAX_X, MAX_Z;
}
public class CameraMovement : MonoBehaviour
{

    public float speed;
    public Boundary boundary; 
   
    void Update()
    {
        Movement();
    }
    void Movement()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        if (z != 0 || x != 0)
        {            
            Camera.main.transform.position += Vector3.forward * z * speed;
            Camera.main.transform.position += Vector3.right * x * speed;
        }
        Camera.main.transform.position = new Vector3(
        Mathf.Clamp(transform.position.x, boundary.MIN_X, boundary.MAX_X),
        20.0f,
        Mathf.Clamp(transform.position.z, boundary.MIN_Z, boundary.MAX_Z));
    }
}

