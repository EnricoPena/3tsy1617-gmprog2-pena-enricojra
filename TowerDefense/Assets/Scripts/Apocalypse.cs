﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Apocalypse : GodPowers {

    public Image fadeImage;
    void Start()
    {
        fadeImage.CrossFadeAlpha(0.0f, 0.1f, true);
    }       
    public override void Effect()
    {
        Obliterate();
    }
    public void Obliterate()
    {
        StartCoroutine(ApocEffect());       
    }
     IEnumerator ApocEffect()
    {
        fadeImage.CrossFadeAlpha(1.0f, 3.0f, true);
        foreach (var monster in GameObject.FindGameObjectsWithTag("Monster"))
        {
            Destroy(monster);
        }
        yield return new WaitForSeconds(3);
    }
}
