﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Enemy : MonoBehaviour {

    public Animator anim;
    public float speed;
    public int bounty;
    public float enemyHealth;
    public float enemyMaxHealth;
    public bool isFlying;
    private Transform target;
    private int waypointIndex = 0;

    void Start()
    {
        anim = GetComponent<Animator>();
        target = Waypoints.waypoints[0];
        anim.SetTrigger("Run");
    }
    void Update()
    {
        transform.LookAt(target);
        Vector3 direction = target.position - transform.position;        
        transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.2f)
        {
            GoToNextWaypoint();
        }
    }
    public void TakeDamage(float amount)
    {
        enemyHealth -= amount;
        if (enemyHealth <= 0)
        {
            anim.SetTrigger("Dead");
            Die();
        }
    }
    void Die()
    {
        PlayerStatus.gold += bounty;
        Destroy(gameObject);
    }
    void GoToNextWaypoint()
    {
        if (waypointIndex >= Waypoints.waypoints.Length - 1)
        {
            EndPath();
            return;
        }
        waypointIndex++;
        target = Waypoints.waypoints[waypointIndex];
    }
    void EndPath()
    {
        Destroy(gameObject);
        PlayerStatus.health--;
    }
}
