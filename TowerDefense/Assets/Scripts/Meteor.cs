﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : GodPowers {

    void Update()
    {
        Effect();
    }
    public override void Effect()
    {        
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Collider[] colliders = Physics.OverlapSphere(Input.mousePosition, areaOfEffect);
            foreach (Collider collider in colliders)
            {
                if (collider.tag == "Monster")
                {
                    Enemy enemy = collider.GetComponent<Enemy>();
                    enemy.enemyHealth *= .15f;
                }
            }
        }               
    }
}
