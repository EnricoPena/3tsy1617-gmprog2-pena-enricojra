﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static bool gameEnded = false;

    void Update()
    {

        if (gameEnded)
        {
            return;
        }

        if (PlayerStatus.health <= 0)
        {
            EndGame();
        }
    }
    void EndGame()
    {
		gameEnded = true; SceneManager.LoadScene (1);
        Debug.Log("Game Over!");

    }
}
