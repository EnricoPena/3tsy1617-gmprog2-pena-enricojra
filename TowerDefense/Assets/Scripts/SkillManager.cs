﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillManager : MonoBehaviour {

    public static SkillManager instance;

    public Text textBox;
    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("Another SkillManager exists!");
            return;
        }
        instance = this;
    }
}
