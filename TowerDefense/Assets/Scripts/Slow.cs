﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slow : Debuff {
    
    void Start()
    {
        Enemy enemy = gameObject.GetComponentInParent<Enemy>();
        enemy.speed *= debuffMagnitude;
        StartCoroutine(DebuffDuration());
    }  
    void OnDestroy()
    {
        if (GetComponentInParent<Enemy>() == null)
        {
            return;
        }
        Enemy enemy = gameObject.GetComponentInParent<Enemy>();
        enemy.speed /= debuffMagnitude;
    }
}
