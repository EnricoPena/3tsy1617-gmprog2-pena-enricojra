﻿using UnityEngine;
[System.Serializable]
public class TowerBlueprint
{
    public GameObject towerPrefab, buildEffect;
    public GameObject[] upgradedPrefab;
    public int cost, upgradeCost;
    public float buildDelay, upgradeDelay;
}
