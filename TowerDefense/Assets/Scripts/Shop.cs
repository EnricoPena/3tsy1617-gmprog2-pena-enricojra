﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour {

    public TowerBlueprint gunTower, missleTower, freezeTower, fireTower;
    public Text textBox; 
    BuildManager buildManager;
    
	void Start()
	{
		buildManager = BuildManager.instance;
	}

	public void SelectGunTower()
	{
		textBox.text = "Gun Tower selected.";
        buildManager.SelectTowerToBuild(gunTower);
	}
	public void SelectMissileTower()
	{
        textBox.text = "Missle Tower selected.";
        buildManager.SelectTowerToBuild(missleTower);
	}
    public void SelectFireTower()
    {
        textBox.text = "Fire Tower selected.";
        buildManager.SelectTowerToBuild(fireTower);
    }
    public void SelectFreezeTower()
    {
        textBox.text = "Freeze Tower selected.";
        buildManager.SelectTowerToBuild(freezeTower);
    }
}
