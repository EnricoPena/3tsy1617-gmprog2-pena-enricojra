﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildManager : MonoBehaviour
{

    // Singleton pattern
    public static BuildManager instance;    
    private TowerBlueprint towerToBuild;
    public NodeUI nodeUI;
    private Node selectedNode;
    public Text textBox;
    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("Another BuildManager exists!");
            return;
        }
        instance = this;
    }    
    public bool CanBuild { get { return towerToBuild != null; } }  
    public bool HasEnoughGold { get { return PlayerStatus.gold >= towerToBuild.cost; } }
    public void SelectTowerToBuild(TowerBlueprint tower)
    {     
        towerToBuild = tower;
        Deselect();
    }
    public void SelectNode(Node node)
    {       
        if (selectedNode == node)
        {
            Deselect();
            return;
        }
        textBox.text = "Tower selected.";
        selectedNode = node;
        towerToBuild = null;
        nodeUI.SetTarget(node);
    }  
    public void Deselect()
    {
        selectedNode = null;
        nodeUI.HideUI();
    }    
    public TowerBlueprint GetTowerToBuild()
    {
        return towerToBuild;
    }
}
	
