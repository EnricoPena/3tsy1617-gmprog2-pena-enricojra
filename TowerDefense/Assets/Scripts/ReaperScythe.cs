﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReaperScythe : GodPowers {

    float enemyHealthDamage;
    Enemy[] monsterList;
    void Update()
    {
        UseSkill();
    }
    public override void Effect()
    {
        monsterList = FindObjectsOfType<Enemy>();
        foreach (var monster in monsterList)
        {
            enemyHealthDamage = monster.enemyHealth / monster.enemyMaxHealth;
            if (enemyHealthDamage <= .15f)
            {                
                Collider[] col = Physics.OverlapSphere(monster.gameObject.transform.position, areaOfEffect);
                foreach (var collider in col)
                {                    
                    if (collider.tag == "Monster")
                    {
                        Enemy _enemy = collider.GetComponent<Enemy>();
                        _enemy.enemyHealth -= enemyHealthDamage * 100;
                    }                    
                }
                Destroy(monster.gameObject);
                Debug.Log("Unit reaped!");
            }
        }               
    }
}
