﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeUI : MonoBehaviour
{
    public GameObject nodeUI;
    private Node target;

    public void SetTarget(Node node)
    {
        target = node;

        transform.position = target.GetBuildPosition();
        nodeUI.SetActive(true);
    }
    public void HideUI()
    {
        nodeUI.SetActive(false);
    }
    public void Upgrade()
    {        
        StartCoroutine(target.UpgradeTower());
        BuildManager.instance.Deselect();
    }

}
