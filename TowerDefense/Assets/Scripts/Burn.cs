﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Burn : Debuff {
    
    void Start()
    {
        InvokeRepeating("DamageOverTime", 0, 1.0f);
        StartCoroutine(DebuffDuration());
    }   
     void DamageOverTime()
    {
        Enemy enemy = gameObject.GetComponentInParent<Enemy>();
        enemy.TakeDamage(debuffMagnitude);
    }
}
