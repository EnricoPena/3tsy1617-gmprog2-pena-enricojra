﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
[System.Serializable]
public class Wave
{
    public string name;
    public Transform enemy;
    public int enemyCount;
    public float spawnRate;
    public float upgradedEnemyHealth;
    public int upgradedEnemyBounty;    
}
public class WaveSpawner : MonoBehaviour
{
    public enum GameState { Spawning, Waiting, Counting };
    private GameState state = GameState.Counting;
    public Wave[] waves;
    public int nextWave;
    public float waveTime = 30f;
    public float waveCountdown;
    public float searchTime = 1f;
    public Text textBox;

    [Header("Text")]
	public Text waveTimerText;
    IEnumerator SpawnWave(Wave _wave)
    {
        state = GameState.Spawning;

        for (int i = 0; i < _wave.enemyCount; i++)
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1 / _wave.spawnRate);
        }

        state = GameState.Waiting;

        yield break;
    }
    void Start()
    {
        waveCountdown = waveTime;
    }

    void Update()
    {        
        waveTimerText.text = "Next wave in: " + waveCountdown.ToString("F0");
        if (state == GameState.Waiting)
        {
			waveCountdown = waveTime;
            if (!EnemyIsAlive())
            {
                textBox.text = "Wave Completed!";
				state = GameState.Counting;
                
				nextWave++;
            }
            else
            {
                return;
            }
        }
        if (waveCountdown <= 0f)
		{
            textBox.text = waves[nextWave].name;
            if (state != GameState.Spawning)
            {
				waveCountdown = waveTime;
                // Spawn enemies
                StartCoroutine(SpawnWave(waves[nextWave]));
            }            
        }
        else
        {
            waveCountdown -= Time.deltaTime;
        }        
    }
    void SpawnEnemy(Transform _enemy)
    {        
        Instantiate(_enemy, transform.position, transform.rotation);
        Enemy enemy = _enemy.GetComponent<Enemy>();
        enemy.enemyHealth = waves[nextWave].upgradedEnemyHealth;
        enemy.enemyMaxHealth = waves[nextWave].upgradedEnemyHealth;
        enemy.bounty = waves[nextWave].upgradedEnemyBounty;                   
    }
    bool EnemyIsAlive()
    {        
        searchTime -= Time.deltaTime;
        if (searchTime <= 0f)
        {
            searchTime = 1f;
            ;
            if (GameObject.FindGameObjectsWithTag("Monster").Length <= 0)
            {                
                return false;
            }
        }
        return true;
    }
}
