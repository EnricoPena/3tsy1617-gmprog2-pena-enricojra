﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Debuff : MonoBehaviour {

    public DebuffReceiver target;
    public float debuffDuration, debuffMagnitude;
    public IEnumerator DebuffDuration()
    {
        yield return new WaitForSeconds(debuffDuration);
        Destroy(gameObject);
    }
}
