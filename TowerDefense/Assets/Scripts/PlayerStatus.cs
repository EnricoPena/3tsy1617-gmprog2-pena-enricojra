﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerStatus : MonoBehaviour {

    public static int gold;
    public int startingGold = 500;
    public Text goldText;
    public Text healthText;
    public static int health;
    public int startingHealth = 30;
    
    void Start()
    {
        gold = startingGold;
        health = startingHealth;
    }
    void Update()
    {
        goldText.text = gold.ToString() + "G";
        healthText.text = "Lives: " + health;
    }

}
