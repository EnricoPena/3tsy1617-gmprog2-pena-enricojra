﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.UI;
[System.Serializable]
public class PlayerAttack : UnityEvent<Behavior> { }
public class GainExp : UnityEvent<Behavior> { }
public class Behavior : MonoBehaviour {

    public enum AnimState
    {
        Idle, Run, AttackRun, Attack, AttackStandby,
        Combo, Skill, Damage, PutBlade, Dead, DrawBlade
    }
    public EnemyBehavior[] enemy;
    public PlayerAttack playerAttack = new PlayerAttack();
    public GainExp ExpGet = new GainExp();
    public Text nameText, levelText, attackText, strText, vitText, hpText, expText; 
    NavMeshAgent agent;
    Animator anim;
    public GameObject marker;
    private AnimState _animState;
    public int str, vit, damage, level;
    public float exp, expRequired, health, maxHealth;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        agent.stoppingDistance = .75f;
        level = 1;
        str = 5;
        vit = 5;
        maxHealth = vit * 10;
        health = maxHealth;        
        damage = str * 2;
        expRequired = 100 * level;
        for (int i = 0; i < enemy.Length; i++)
        {
            enemy[i].combat.AddListener(TakeDamage);
            enemy[i].monsterKilled.AddListener(GainExp);
        }
    }     
    void Update()
    {
        Idle();
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Move();
        }
        UpdateText();
        if (exp >= expRequired)
        {
            LevelUp();
        }
    }
    void Move()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;        
        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log(hit.collider.tag);
            if (hit.collider.gameObject.tag == "Field")
            {
                _animState = AnimState.Run;
                anim.SetInteger("AnimIndex", (int)_animState);
                if (!marker.activeInHierarchy)
                {
                    marker.SetActive(true);
                }
                marker.transform.position = hit.point;
                agent.SetDestination(hit.point);
            }
            if (hit.collider.gameObject.tag == "Monster")
            {
                _animState = AnimState.AttackRun;
                anim.SetInteger("AnimIndex", (int)_animState);
                agent.SetDestination(hit.collider.transform.position);
                if (Vector3.Distance(transform.position, hit.collider.transform.position) <= 2f)
                {
                    Attack(hit.collider.gameObject);
                }
            }           
        }               
    }
    void Attack(GameObject enemy)
    {
        transform.LookAt(enemy.transform);
        _animState = AnimState.Attack;
        anim.SetInteger("AnimIndex", (int)_animState);
    }
    void Idle()
    {
        if (Vector3.Distance(transform.position, marker.transform.position) <= .75f)
        {
            _animState = AnimState.Idle;
            anim.SetInteger("AnimIndex", (int)_animState);
        }
    }
    void TakeDamage(EnemyBehavior enemy)
    {
        _animState = AnimState.Damage;
        anim.SetInteger("AnimIndex", (int)_animState);
        health -= enemy.damage;
    }
    public void DamageTo(GameObject target)
    {
        playerAttack.Invoke(this);
    }    
    void GainExp(EnemyBehavior enemy)
    {
        exp += enemy.expReward;
        ExpGet.Invoke(this);
    }
    void UpdateText()
    {
        nameText.text = "Name: " + gameObject.name;
        levelText.text = level.ToString();
        attackText.text = "Attack: " + damage;
        strText.text = "Strength: " + str;
        vitText.text = "Vitality: " + vit;
        expText.text = "EXP: " + exp + "/" + expRequired;
        hpText.text = "HP: " + health + "/" + maxHealth;
    }
    void LevelUp()
    {
        exp = 0;
        level = 2;
        str += 1;
        vit += 1;
        damage = str * 2;
        maxHealth = vit * 10;
        health = maxHealth;
        expRequired = 100 * level;

    }

    void StartEffect()
    {

    }
    void StopEffect()
    {

    }
}
