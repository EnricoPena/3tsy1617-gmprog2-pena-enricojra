﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
[System.Serializable]
public class Combat : UnityEvent<EnemyBehavior> { }
public class OnMonsterKilled : UnityEvent<EnemyBehavior> { }
public class EnemyBehavior : MonoBehaviour {

    public enum EnemyAnimState
    {
        Idle, Walk, AttackRun, Attack, 
        Damage, Skill, Dead
    }
    [HideInInspector] public int str, vit, health, damage, expReward;
    [HideInInspector] public Behavior player;
    [HideInInspector] public Combat combat = new Combat();
    [HideInInspector] public OnMonsterKilled monsterKilled = new OnMonsterKilled();
    [HideInInspector] public EnemyAnimState enemyAnimState;
    [HideInInspector] public Animator enemyAnim;
    [HideInInspector] public NavMeshAgent agent;
    [HideInInspector] public float chaseTimerLimit = 5f, idleTimerLimit = 3f;
    [HideInInspector] public GameObject target;
    [HideInInspector] public IEnemyState currentState;
    [HideInInspector] public IdleState idleState;
    [HideInInspector] public ChaseState chaseState;
    [HideInInspector] public PatrolState patrolState;
    [HideInInspector] public AttackState attackState;

    void Awake()
    {
        idleState = new IdleState(this);
        chaseState = new ChaseState(this);
        attackState = new AttackState(this);
        patrolState = new PatrolState(this);        
    }
    void Start()
    {
        currentState = idleState;
        player.playerAttack.AddListener(TakeDamage);
    } 
    void Update()
    {
        currentState.UpdateState();
        if (health <= 0)
        {
            Die();
        }
    }
    public void Animate(int animIndex)
    {
        enemyAnim.SetInteger("EnemyIndex", animIndex);
    }    
    public void TakeDamage(Behavior player)
    {
        health -= player.damage;
    }    
    public void DamageTo(GameObject target)
    {
        combat.Invoke(this);
    }
    public void Die()
    {
        Destroy(gameObject);
        monsterKilled.Invoke(this);
    }
}
