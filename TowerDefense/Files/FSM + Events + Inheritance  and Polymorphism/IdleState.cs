﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IEnemyState
{

    private readonly EnemyBehavior enemy;
    private float searchRadius = 5.0f, idleTimer = 0;

    public IdleState(EnemyBehavior enemyBehavior)
    {
        enemy = enemyBehavior;
    }
    public void UpdateState()
    {
        Idle();
    }
    public void ToPatrolState()
    {
        Debug.Log("Patrol State");
        enemy.currentState = enemy.patrolState;
    }

    public void ToChaseState()
    {
        enemy.currentState = enemy.chaseState;
    }

    public void ToIdleState()
    {
        Debug.Log("Can't transition to same state");
    }
    public void ToAttackState()
    {
        Debug.Log("Can't transition from Idle to Attack State.");
    }
    private void Idle()
    {
        enemy.enemyAnimState = EnemyBehavior.EnemyAnimState.Idle;
        enemy.Animate((int)enemy.enemyAnimState);
        idleTimer += Time.deltaTime;
        if (idleTimer >= enemy.idleTimerLimit)
        {            
            idleTimer = 0;
            ToPatrolState();
        }
        Collider[] col = Physics.OverlapSphere(enemy.gameObject.transform.position, searchRadius);
        foreach (var colliders in col)
        {
            if (colliders.tag == "Player")
            {
                enemy.target = colliders.gameObject;
                ToChaseState();
            }
        }
    }
}
