﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyState {

    void UpdateState();
    void ToIdleState();
    void ToPatrolState();
    void ToChaseState();
    void ToAttackState();
}
