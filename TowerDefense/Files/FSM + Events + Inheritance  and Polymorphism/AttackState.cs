﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : IEnemyState {

    private readonly EnemyBehavior enemy;
    public AttackState(EnemyBehavior enemyBehavior)
    {
        enemy = enemyBehavior;
    }

    public void UpdateState()
    {
        Attack();
    }
    public void ToPatrolState()
    {
        Debug.Log("Can't transition from Attack to Patrol state.");
    }

    public void ToChaseState()
    {
        enemy.currentState = enemy.chaseState;
    }

    public void ToIdleState()
    {
        Debug.Log("Can't transition from Attack to Idle state");
    }
    public void ToAttackState()
    {
        Debug.Log("Can't transition to same state.");
    }
    private void Attack()
    {
        if (Vector3.Distance(enemy.transform.position, enemy.target.transform.position) >= 3f)
        {
            ToChaseState();
        }        
        enemy.transform.LookAt(enemy.target.transform);
        enemy.enemyAnimState = EnemyBehavior.EnemyAnimState.Attack;        
        enemy.Animate((int)enemy.enemyAnimState);
    }
}
