﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseState : IEnemyState {

    private readonly EnemyBehavior enemy;
    private float chaseTimer = 0;

    public ChaseState(EnemyBehavior enemyBehavior)
    {
        enemy = enemyBehavior;
    }
    public void UpdateState()
    {
        Chase();
    }    
    public void ToIdleState()
    {
        Debug.Log("Can't transition from Chase to Idle state.");
    }
    public void ToPatrolState()
    {
        enemy.currentState = enemy.patrolState;
    }
    public void ToChaseState()
    {
        Debug.Log("Can't transition to same state");
    }
    public void ToAttackState()
    {
        Debug.Log("Attack State");
        enemy.currentState = enemy.attackState;
    }
    private void Chase()
    {
        enemy.transform.LookAt(enemy.target.transform);
        enemy.enemyAnimState = EnemyBehavior.EnemyAnimState.AttackRun;
        enemy.Animate((int)enemy.enemyAnimState);
        enemy.agent.SetDestination(enemy.target.transform.position);
        chaseTimer += Time.deltaTime;
        if (Vector3.Distance(enemy.transform.position, enemy.target.transform.position) <= 1.5f)
        {
            ToAttackState();
        }
        if (chaseTimer >= enemy.chaseTimerLimit)
        {
            ToPatrolState();
        }
    }
}
