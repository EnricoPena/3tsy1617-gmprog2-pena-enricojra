﻿using UnityEngine;
using System.Collections;

public class PatrolState : IEnemyState
{

    private readonly EnemyBehavior enemy;
    private bool hasPatrolled = false;
    private float searchRadius = 5.0f;

    public PatrolState(EnemyBehavior enemyBehavior)
    {
        enemy = enemyBehavior;
    }
    public void UpdateState()
    {
        Patrol();
    }
    public void ToIdleState()
    {
        Debug.Log("Idle State.");
        hasPatrolled = false;
        enemy.currentState = enemy.idleState;
    }

    public void ToPatrolState()
    {
        Debug.Log("Can't transition to same state.");
    }

    public void ToChaseState()
    {
        Debug.Log("Chase State.");
        enemy.currentState = enemy.chaseState;
    }
    public void ToAttackState()
    {
        Debug.Log("Can't transition from Patrol to Attack State.");
    }

    private void Patrol()
    {
        if (!hasPatrolled)
        {
            enemy.enemyAnimState = EnemyBehavior.EnemyAnimState.Walk;
            enemy.Animate((int)enemy.enemyAnimState);
            enemy.agent.SetDestination(enemy.transform.position + new Vector3(Random.Range(-3.0f, 3.0f), 0.0f, Random.Range(-3.0f, 3.0f)));
            hasPatrolled = true;
        }
        if (hasPatrolled)
        {
            ToIdleState();
        }
        Collider[] col = Physics.OverlapSphere(enemy.gameObject.transform.position, searchRadius);
        foreach (var colliders in col)
        {
            if (colliders.tag == "Player")
            {
                enemy.target = colliders.gameObject;
                ToChaseState();
            }
        }         
        
    }
}