﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodPowers : MonoBehaviour {
    
    public float areaOfEffect, timer;
    public bool cooldown;    
    public virtual void UseSkill()
    {
        if (cooldown)
        {
            Debug.Log("Skill is on cooldown!");
            return;
        }
        else if (!cooldown)
        {
            cooldown = true;
            Effect();
            Debug.Log("Skill used.");
            Invoke("Cooldown", timer);
        }
             
    }
    public virtual void Effect()
    {

    }
    public virtual void Cooldown()
    {
        cooldown = false;
    }
}
