﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReaperScythe : GodPowers {

    float enemyHealthDamage;

    void Update()
    {
        UseSkill();
    }
    public override void Effect()
    {
        List<GameObject> monsterList = new List<GameObject>();
        monsterList.AddRange(GameObject.FindGameObjectsWithTag("Monster"));
        foreach (var monster in monsterList)
        {
            Enemy enemy = monster.GetComponent<Enemy>();
            enemyHealthDamage = enemy.enemyHealth / enemy.enemyMaxHealth;
            if (enemyHealthDamage <= .15f)
            {
                Destroy(monster);
                Debug.Log("Unit reaped!");
                Collider[] col = Physics.OverlapSphere(monster.transform.position, areaOfEffect);
                foreach (var affectedMonster in col)
                {
                    Enemy _enemy = affectedMonster.GetComponent<Enemy>();
                    _enemy.enemyHealth -= enemyHealthDamage;
                }
            }
        }               
    }
}
