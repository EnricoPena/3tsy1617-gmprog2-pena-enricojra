﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : GodPowers {

    public GameObject skillPrefab;  
    public override void Effect()
    {        
        Collider[] colliders = Physics.OverlapSphere(skillPrefab.transform.position, areaOfEffect);
        foreach (Collider collider in colliders)
        {            
            if (collider.tag == "Monster")
            {
                Blast(collider);
            }
        }        
    }
    public void Blast(Collider collider)
    {
        Enemy e = collider.GetComponent<Enemy>();
        e.enemyHealth -= (e.enemyHealth * .15f);
    }    
}
